//
//  main.m
//  Tippspiel
//
//  Created by Felix on 16.05.13.
//  Copyright (c) 2013 Felix Wehnert. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
	return NSApplicationMain(argc, (const char **)argv);
}
