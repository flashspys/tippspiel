//
//  KeyWindow.m
//  Tippspiel
//
//  Created by Felix on 16.05.13.
//  Copyright (c) 2013 Felix Wehnert. All rights reserved.
//

#import "KeyWindow.h"
#import "ViewController.h"

@implementation KeyWindow
@synthesize controller;


-(void)keyDown:(NSEvent *)theEvent {
	if([theEvent keyCode] == 36) { // Enter Key
		[super keyDown:theEvent];
	} else {	
		[controller keyDown:theEvent];
	}
}

@end
