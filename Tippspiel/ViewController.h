//
//  ViewController.h
//  Tippspiel
//
//  Created by Felix on 16.05.13.
//  Copyright (c) 2013 Felix Wehnert. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "NSString+additions.h"
@class Statistic;

@interface ViewController : NSViewController

@property IBOutlet NSProgressIndicator* progressIndicator;
@property IBOutlet NSTextField* charList;
@property IBOutlet NSButton* pushButton;
@property IBOutlet NSTextField* points;
@property IBOutlet NSTextField* name;

@property IBOutlet NSTextField* wrong;
@property IBOutlet NSTextField* correct;
@property IBOutlet NSTextField* ratio;

-(void)keyDown:(NSEvent *)theEvent;


-(IBAction)buttonPushed:(id)sender;
-(IBAction)highscorePushed:(id)sender;


@end
