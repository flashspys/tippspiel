//
//  NSString+additions.m
//  Tippspiel
//
//  Created by Felix on 16.05.13.
//  Copyright (c) 2013 Felix Wehnert. All rights reserved.
//

#import "NSString+additions.h"

@implementation NSString (additions)

-(NSString *)stringAtIndex:(NSUInteger)loc {
	
	@try {
		return [self substringWithRange:NSMakeRange(loc, 1)];

	}
	@catch (NSException *exception) {
		return @"";
	}
	
}

@end
