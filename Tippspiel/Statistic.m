//
//  Statistic.m
//  Tippspiel
//
//  Created by Felix on 16.05.13.
//  Copyright (c) 2013 Felix Wehnert. All rights reserved.
//

#import "Statistic.h"

@implementation Statistic

@synthesize wrong, correct;

- (id)init
{
    self = [super init];
    if (self) {
        correct = 0;
		wrong = 0;
    }
    return self;
}

-(void)incrementCorrect {
	correct++;
}

-(void)incrementWrong {
	wrong++;
}

-(NSUInteger)total {
	return self.wrong + self.correct;
}

-(float)ratio {
	if(correct + wrong == 0)
		return 0.0f;
	return (float)correct / (float)(correct + wrong);
}

-(void)reset {
	correct = 0;
	wrong = 0;
}


-(NSUInteger)calculatePoints {
	if((correct * 10) < (wrong * 5)) {
		return 0;
	}
	return (correct * 10) - (wrong * 5);
}



@end
