//
//  Highscore.h
//  Tippspiel
//
//  Created by Felix on 16.05.13.
//  Copyright (c) 2013 Felix Wehnert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Highscore : NSObject

+(id)sharedInstance;


-(void)setHighscore:(NSNumber*)score forUser:(NSString*) user;
-(NSDictionary*)getScores;

-(void)save;

@end
