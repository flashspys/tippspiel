//
//  ViewController.m
//  Tippspiel
//
//  Created by Felix on 16.05.13.
//  Copyright (c) 2013 Felix Wehnert. All rights reserved.

#import "ViewController.h"
#import "Statistic.h"
#import <AppKit/AppKit.h>
#import "KeyWindow.h"
#import "Highscore.h"

@interface ViewController ()

@property (strong) NSTimer* timer;
@property (strong) Statistic* stat;

@property (readonly) int status; // 0 -> aus 1-> an :O

@property float currentTimerInterval;

@end

@implementation ViewController

@synthesize progressIndicator, charList, points, pushButton, name, correct, wrong, ratio;
@synthesize timer, stat, currentTimerInterval, status;


-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self) {
		
		stat = [[Statistic alloc] init];
		
    }
    return self;
}

-(void)awakeFromNib {
	[super awakeFromNib];

	[self clear];
}


-(void)fired {
	NSString* chars = [charList stringValue];
	[charList setStringValue:[NSString stringWithFormat:@"%@%@", chars, [self randomChar]]];
	chars = [charList stringValue];
	if([chars length] > 7 ) {
		[self gameover];
		return;
	}
	
	if (currentTimerInterval > 400)
		currentTimerInterval -= 10.0f;
	if (currentTimerInterval > 250)
		currentTimerInterval -= 7.0f;
	if (currentTimerInterval < 100)
		currentTimerInterval -= 2.0f;
	[progressIndicator setDoubleValue:((double)(800 - currentTimerInterval))];
	[progressIndicator setNeedsDisplay:YES];

	[timer invalidate];
	timer = nil;
	timer = [NSTimer scheduledTimerWithTimeInterval:currentTimerInterval/1000.0f
											 target:self
										   selector:@selector(fired)
										   userInfo:nil
											repeats:NO];
}

-(void)keyDown:(NSEvent *)theEvent {
	if(!status) {
		return;
	}
	
	NSString* chars = [charList stringValue];
	NSString* pressedChar = [[theEvent charactersIgnoringModifiers] uppercaseString];
	if([chars isEqualToString:@""]) {
		return;
	}
	
	if([[chars stringAtIndex:0] isEqualToString:pressedChar]) {
		[charList setStringValue:[[charList stringValue] substringFromIndex:1]];
		[stat incrementCorrect];
	} else {
		[stat incrementWrong];
	}
	
	[self updateStats];
}


-(void)buttonPushed:(NSButton*)sender {
	if([[sender title] isEqualToString:@"Start"]) {
		[self clear];
		
		[self.view.window makeFirstResponder:sender];
		
		[name resignFirstResponder];
		[name setSelectable:NO];
		[name setEditable:NO];
		
		[sender setTitle:@"Stop"];
		
		
		timer = [NSTimer scheduledTimerWithTimeInterval:currentTimerInterval/1000.0f
												 target:self
											   selector:@selector(fired)
											   userInfo:nil
												repeats:NO];
		
		status = 1;
		
	} else {
		[self gameover];
	}
	
}


-(void)highscorePushed:(id)sender {
	
	NSMutableString* string = [NSMutableString stringWithString:@"\n"];
	
	Highscore* hs = [Highscore sharedInstance];
	NSDictionary* scores = [hs getScores];
	
	NSArray* sortedScores = [[scores allValues] sortedArrayUsingSelector:@selector(compare:)];
	
	for (NSNumber* score in [sortedScores reverseObjectEnumerator]) {
		for (NSString* user in [scores allKeysForObject:score]) {
			[string appendFormat:@"%@ | %@\n", user, score];
		}
	}

	NSLog(string);

}

#pragma mark Private

-(NSString*)randomChar {
	NSString *alphabet  = @"ABCDEFGHIJKLMNOPQRSTUVWXZY";
	return [NSString stringWithFormat:@"%C", [alphabet characterAtIndex:arc4random() % [alphabet length]]];
}


-(void)updateStats {
	
	NSDecimalNumber *t1 = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f", [stat ratio]]];
	NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
	[formatter setNumberStyle:NSNumberFormatterPercentStyle];
	NSString *formattedOutput = [formatter stringFromNumber:t1];
	
	NSString* w = [NSString stringWithFormat:@"%lu/%lu", [stat wrong], [stat total]];
	NSString* c = [NSString stringWithFormat:@"%lu/%lu", [stat correct], [stat total]];
	
	[wrong setStringValue:w];
	[correct setStringValue:c];
	[ratio setStringValue:formattedOutput];
	
	[points setStringValue:[NSString stringWithFormat:@"%lu", (unsigned long)[stat calculatePoints]]];
	
}

-(void)gameover {
	status = 0;
	[timer invalidate];
	timer = nil;
	[charList setStringValue:@"Game Over"];
	
	Highscore* hs = [Highscore sharedInstance];
	[hs setHighscore:[NSNumber numberWithUnsignedInteger:[stat calculatePoints]] forUser:[name stringValue]];
	[hs save];
	
	
	[pushButton setTitle:@"Start"];	
	[self.view.window makeFirstResponder:name];
	[name setStringValue:@""];
	[name setSelectable:YES];
	[name setEditable:YES];
}


-(void)clear {
	[pushButton setTitle:@"Start"];
	
	[name setSelectable:YES];
	[name setEditable:YES];
	
	[charList setStringValue:@""];
	[points setStringValue:@"0"];
	[correct setStringValue:@""];
	[wrong setStringValue:@""];
	[ratio setStringValue:@""];
	[charList setStringValue:@""];
	
	[progressIndicator setUsesThreadedAnimation:YES];
	
	[stat reset];
	timer = nil;
	currentTimerInterval = 800.0f;
	[progressIndicator setDoubleValue:0.0];
	
	[self updateStats];
	status = 0;
	
	
}

@end