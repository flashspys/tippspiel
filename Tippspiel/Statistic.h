//
//  Statistic.h
//  Tippspiel
//
//  Created by Felix on 16.05.13.
//  Copyright (c) 2013 Felix Wehnert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Statistic : NSObject

@property (readonly) NSUInteger correct;
@property (readonly) NSUInteger wrong;

-(NSUInteger)total;
-(float)ratio;
-(void)incrementWrong;
-(void)incrementCorrect;
-(void)reset;

-(NSUInteger)calculatePoints;

@end
