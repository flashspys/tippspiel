//
//  NSString+additions.h
//  Tippspiel
//
//  Created by Felix on 16.05.13.
//  Copyright (c) 2013 Felix Wehnert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (additions)

-(NSString*)stringAtIndex:(NSUInteger)loc;

@end
