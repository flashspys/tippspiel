//
//  Highscore.m
//  Tippspiel
//
//  Created by Felix on 16.05.13.
//  Copyright (c) 2013 Felix Wehnert. All rights reserved.
//

#import "Highscore.h"

@interface Highscore ()

@property (strong) NSMutableDictionary* highscores;

@end

@implementation Highscore

@synthesize highscores;

static Highscore *score = NULL;

+ (Highscore *)sharedInstance
{
    @synchronized(self)
    {
        if (score == NULL)
            score = [[self alloc] init];
    }
	
    return score;
}

- (id)init
{
    self = [super init];
    if (self) {
		NSString     * path        = [self pathForDataFile];
		NSMutableDictionary * rootObject;
		
		rootObject = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
		highscores = (NSMutableDictionary*)rootObject;
		
		if(highscores == nil) {
			highscores = [NSMutableDictionary dictionary];
		}
    }
    return self;
}



-(NSDictionary*)getScores {
	return highscores;
}

-(void)setHighscore:(NSNumber*)score forUser:(NSString *)user {
	if([highscores objectForKey:user] != nil) {
		if([[highscores objectForKey:user] intValue] < [score intValue]) {
			[highscores setObject:score forKey:user];
		}
	} else {
		[highscores setObject:score forKey:user];
	}
}

#pragma mark Saving

- (NSString *) pathForDataFile
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES);
	NSString *applicationSupportDirectory = [paths objectAtIndex:0];
	applicationSupportDirectory = [applicationSupportDirectory stringByAppendingPathComponent:@"/me.wehnert.felix.Tippspiel/"];
	
	if ([fileManager fileExistsAtPath: applicationSupportDirectory] == NO)
	{
		[fileManager createDirectoryAtPath:applicationSupportDirectory withIntermediateDirectories:NO attributes:nil error:nil];
	}
    
	NSString *fileName = @"/High.scores";
	
	return [applicationSupportDirectory stringByAppendingPathComponent: fileName];
}


-(void)save {
	NSString * path = [self pathForDataFile];
	if([NSKeyedArchiver archiveRootObject:highscores toFile: path]) {
		NSLog(@"Successfully saved");
	} else {
		NSLog(@"BUM! Saving Error");
	}
}


@end
